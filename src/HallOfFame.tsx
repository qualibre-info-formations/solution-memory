import React, {FunctionComponent, useEffect, useState} from 'react'

import './HallOfFame.css'

export interface HofEntry {
    date: string
    guesses: number
    id: number
    player: string
}

const API_KEY = '5d796281b5eedc635380d9b4';

const HallOfFame: FunctionComponent = () => {
    const [entries, setEntries] = useState<HofEntry[]>([]);
    useEffect(() => {
        fetch('https://memoryhalloffame-9cee.restdb.io/rest/halloffame', {
            headers: {
                'cache-control': 'no-cache',
                'x-apikey': API_KEY,
            },
        })
            .then((result) => {
                if (result.ok) {
                    return result.json()
                }
            })
            .then((data) => {
                setEntries(data);
            })
    }, []);

    return (
        <table className="hallOfFame">
            <tbody>
            {
                entries.map(({date, guesses, id, player}: HofEntry) => (
                    <tr key={id}>
                        <td className="date">{date}</td>
                        <td className="guesses">{guesses}</td>
                        <td className="player">{player}</td>
                    </tr>
                ))
            }
            </tbody>
        </table>
    )
}

export default HallOfFame

// == Internal helpers ==============================================

export const FAKE_HOF = [
  { id: 3, guesses: 18, date: '10/10/2017', player: 'Jane' },
  { id: 2, guesses: 23, date: '11/10/2017', player: 'Kevin' },
  { id: 1, guesses: 31, date: '06/10/2017', player: 'Louisa' },
  { id: 0, guesses: 48, date: '14/10/2017', player: 'Marc' },
]

export function saveHOFEntry(entry: HofEntry, onStored: Function) {
    entry.date = new Date().toLocaleDateString();
    entry.id = Date.now();

    fetch('https://memoryhalloffame-9cee.restdb.io/rest/halloffame', {
        method: 'POST',
        headers: {
            'cache-control': 'no-cache',
            "Content-Type": "application/json",
            'x-apikey': API_KEY,
        },
        body: JSON.stringify(entry)
    })
        .then((result) => {
            if (result.ok) {
                onStored();
            }
        })
        .catch((err) => {
            console.error(err);
        })
}