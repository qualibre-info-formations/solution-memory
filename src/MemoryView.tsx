import React, {FunctionComponent} from 'react';
import './MemoryView.css';
import Card from './Card';
import GuessCount from './GuessCount';
import {HofEntry} from './HallOfFame';
import HighScoreInput from './HighScoreInput';
import {useNavigate} from 'react-router-dom';
import { connect, useSelector, TypedUseSelectorHook } from 'react-redux';
import { RootState } from './store/configureStore';
import { useAppSelector } from './store/hooks';
import { handleCardClick } from './actions/cards';
import { ThunkAction } from 'redux-thunk';

const MemoryView: FunctionComponent<{}> = () => {
    const matchedCardIndices = useAppSelector(state => state.cards.matchedCardIndices);
    const currentPair = useAppSelector(state => state.cards.currentPair);
    const guesses = useAppSelector(state => state.cards.guesses);
    const cards = useAppSelector(state => state.cards.cards);
    const navigate = useNavigate();

    const getFeedbackForCard = (index: number) => {
        const indexMatched = matchedCardIndices.includes(index);

        if (currentPair.length < 2) {
            return indexMatched || index === currentPair[0] ? 'visible' : 'hidden';
        }

        if (currentPair.includes(index)) {
            return indexMatched ? 'justMatched' : 'justMismatched';
        }

        return indexMatched ? 'visible' : 'hidden';
    }

    const onStored = () => {
        navigate('/halloffame');
    }

    const won = cards.length > 0 && matchedCardIndices.length === cards.length;
    return (
        <div className="memory">
            <GuessCount guesses={guesses}/>
            {cards.map((card: string, index: number) => (
                <Card card={card} feedback={getFeedbackForCard(index)} key={index} index={index} />
            ))}
            {won && <HighScoreInput guesses={guesses} onStored={onStored} />}
        </div>
    )
}

export default MemoryView;