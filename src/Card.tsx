import React, {FunctionComponent} from 'react'
import PropTypes from 'prop-types'
import {useAppDispatch } from './store/hooks';

import './Card.css'
import { handleCardClick } from './actions/cards';

const HIDDEN_SYMBOL = '❓';

const Card: FunctionComponent<CardProps> = ({ card, feedback, index }: any) => {
    const dispatch = useAppDispatch();

    return (<>
        {/* @ts-ignore */}
        <div className={`card ${feedback}`} onClick={() => dispatch(handleCardClick(index))}>
    <span className="symbol">
      {feedback === "hidden" ? HIDDEN_SYMBOL : card}
    </span>
        </div>
    </>);
}

export default Card

const propTypes = {
    card: PropTypes.string.isRequired,
    feedback: PropTypes.oneOf([
        'hidden',
        'justMatched',
        'justMismatched',
        'visible'
    ]).isRequired,
    index: PropTypes.number.isRequired
}

type CardProps = PropTypes.InferProps<typeof propTypes>;

Card.propTypes = propTypes;