import PropTypes from 'prop-types';
import React, {ChangeEvent, FormEvent, Component } from 'react';

import './HighScoreInput.css';

import { saveHOFEntry, HofEntry } from './HallOfFame';

interface HighScoreInputState {
  winner: string
}

class HighScoreInput extends Component<HighScoreInputProps, HighScoreInputState> {
  state = {
    winner: ''
  };

  // Arrow func for binding
  handleWinnerUpdate = (event: ChangeEvent<HTMLInputElement>) => {
      this.setState({winner: event.target.value.toUpperCase()});
  }

  // Arrow fx for binding
  persistWinner = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const newEntry: HofEntry = { guesses: this.props.guesses, player: this.state.winner, date: '', id: 0 };
    saveHOFEntry(newEntry, () => {
      this.props.onStored();
    })
  }
  static propTypes: {};

  render() {
    return (
      <form className="highScoreInput" onSubmit={this.persistWinner}>
        <p>
          <label>
            Bravo ! Entre ton prénom :
            <input type="text" autoComplete="given-name"
                   onChange={this.handleWinnerUpdate}
                   value={this.state.winner}/>
          </label>
          <button type="submit">J’ai gagné !</button>
        </p>
      </form>
    )
  }
}

const propTypes = {
  guesses: PropTypes.number.isRequired,
  onStored: PropTypes.func.isRequired
}

type HighScoreInputProps = PropTypes.InferProps<typeof propTypes>;

HighScoreInput.propTypes = propTypes;

export default HighScoreInput
