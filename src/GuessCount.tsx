import React, {FunctionComponent} from 'react'
import PropTypes from 'prop-types'

import './GuessCount.css'

const GuessCount: FunctionComponent<GuessCountProps> = ({ guesses }) => <div className="guesses">{guesses}</div>

export default GuessCount

const propTypes = {
    guesses: PropTypes.number.isRequired
}

type GuessCountProps = PropTypes.InferProps<typeof propTypes>;

GuessCount.propTypes = propTypes;