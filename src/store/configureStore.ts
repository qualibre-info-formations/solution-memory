import { createStore, applyMiddleware, combineReducers } from 'redux';
import storeCards from './reducers/cardsReducer';
import storeHallOfFame from './reducers/hofReducer';
import thunk from 'redux-thunk';

const store = createStore(combineReducers({cards: storeCards, hof: storeHallOfFame}), applyMiddleware(thunk))
export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch