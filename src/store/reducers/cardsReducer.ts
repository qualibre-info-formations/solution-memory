import { AnyAction } from 'redux';

interface CardsReducerState {
    cards: string[],
    currentPair: number[],
    guesses: number,
    matchedCardIndices: number[],
}

const initialState: CardsReducerState = {
    cards: [],
    currentPair: [],
    guesses: 0,
    matchedCardIndices: []
};

export default function storeCards(state = initialState, action: AnyAction) {
    switch (action.type) {
        case 'STORE_CARDS':
            return {...state, cards: action.cards};
        case 'CLEAR_CURRENT_PAIR':
            return {...state, currentPair: []};
        case 'INCREASE_GUESSES':
            return {...state, guesses: state.guesses + 1};
        case 'STORE_MATCHED':
            return {...state, matchedCardIndices: [...state.matchedCardIndices, ...action.cards]};
        case 'STORE_IN_CURRENT_PAIR':
            return {...state, currentPair: [...state.currentPair, action.index]};
        default:
            return state;
    }
}
