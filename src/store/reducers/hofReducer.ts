import {AnyAction} from 'redux';
import {HofEntry} from '../../HallOfFame';

const initialState = { hallOfFame: [] };

interface HofReducerState {
    hallOfFame: HofEntry[]
}

export default function storeHallOfFame(state = initialState, action: AnyAction) {
    switch (action.type) {
        case 'STORE_HOF':
            return {hallOfFame: action.hallOfFame};
        default:
            return state
    }
}