import { render, screen } from '@testing-library/react'
import React from 'react';
import GuessCount from './GuessCount';

describe('<GuessCount guesses={3} />',() => {
    test('renders without crashing', () => {
        render(<GuessCount guesses={3} />);
    });
    test('renders 3 in GuestCount', () => {
        render(<GuessCount guesses={3} />);
        expect(screen.getByText(/3/)).toBeInTheDocument();
    })
})
