import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Card from './Card';

test('renders without crashing', () => {
    render(<Card  card="😁" feedback="hidden" index={3} onClick={() => {}} />);
});
test('should call onClick with appropriate parameter', async () => {
    const onClick = jest.fn()
    render(
        <Card card="😁" feedback="hidden" index={3} onClick={onClick} />
    )
    userEvent.click(screen.getByText('❓'));
    expect(onClick).toHaveBeenCalledTimes(1);
    expect(onClick).toHaveBeenCalledWith(3);
})
test('should display hidden symbol on hidden feedback', async () => {
    render(
        <Card card="😁" feedback="hidden" index={3} onClick={() => {}} />
    )
    expect(screen.getByText(/❓/)).toBeInTheDocument();
})