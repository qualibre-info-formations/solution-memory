import HallOfFame from "../HallOfFame";

export default function HOF(props: {}) {
    return (
        <HallOfFame {...props} />
    );
}