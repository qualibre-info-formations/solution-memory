import MemoryView from "../MemoryView";

export default function Memory(props: {}) {
    return (
        <MemoryView {...props} />
    );
}