import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import fetchMock from 'jest-fetch-mock';

beforeEach(() => {
    require('jest-fetch-mock').enableMocks();
    // @ts-ignore
    fetchMock.doMockIf(/^https?:\/\/memory-api.qualibre-formations.fr\/cards$/, JSON.stringify({
        cards: ["😀","🎉","💖","🎩","🐶","🐱","🦄","🐬","🌍","🌛","🌞","💫","🍎","🍌","🍓","🍐","🍟","🍿"]
    }));
});

test('renders 36 cards after fetching API', async () => {
    // @ts-ignore
    render(<App />);
    const cards = await screen.findAllByText(/❓/);
    expect(cards).toHaveLength(36);
})