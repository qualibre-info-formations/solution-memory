import {AppDispatch, RootState} from '../store/configureStore';
import shuffle from 'lodash.shuffle';
import { ThunkAction } from 'redux-thunk';
import { AnyAction } from 'redux';

const SIDE = 2;
const VISUAL_PAUSE_MSECS = 750;

export interface CardAPIContent {
    cards: string[]
};

const HEADERS = {
    'cache-control': 'no-cache',
};

function generateCards(cards: string[]) {
    const result: Array<string> = [];
    const size = SIDE * SIDE;
    const candidates = shuffle(cards);
    while (result.length < size) {
        const card = candidates.pop();
        if (card) {
            result.push(card, card);
        }
    }
    return shuffle(result);
}

export function fetchCards(): ThunkAction<void, RootState, unknown, AnyAction> {
    return async (dispatch: AppDispatch) => {
        try {
            const apiResult = await fetch(
                'https://memory-api.qualibre-formations.fr/cards',
                {
                    method: 'GET',
                    headers: HEADERS,
                },
            );
            if (!apiResult.ok) {
                throw new Error(`Error - ${apiResult.statusText}`);
            }
            const apiJsonContent: CardAPIContent = await apiResult.json();
            dispatch(storeCards(generateCards(apiJsonContent.cards)));
        } catch (e) {
            console.error(e)
        }
    }
}

export const handleCardClick = (index: number): ThunkAction<void, RootState, unknown, AnyAction> => {
    return (dispatch: AppDispatch, getState: () => RootState) => {
        const { currentPair, matchedCardIndices, cards } = getState().cards;

        if (currentPair.length === 2 || currentPair.includes(index) || matchedCardIndices.includes(index)) {
            return
        }

        dispatch(storeInCurrentPair(index))
        if (currentPair.length === 0) {
            return
        }

        const newPair = [currentPair[0], index]
        const matched = cards[newPair[0]] === cards[newPair[1]]
        dispatch(increaseGuesses())
        if (matched) {
            dispatch(storeInMatchedCardIndices(newPair))
        }
        setTimeout(() => dispatch(clearCurrentPair()), VISUAL_PAUSE_MSECS)
    }
}

export const storeCards = (cards: string[]) => {
    return { type: "STORE_CARDS", cards }
}

export const increaseGuesses = () => {
    return { type: "INCREASE_GUESSES" }
}

export const storeInMatchedCardIndices = (cards: number[]) => {
    return { type: "STORE_MATCHED", cards }
}

export const storeInCurrentPair = (index: number) => {
    return { type: "STORE_IN_CURRENT_PAIR", index }
}

export const clearCurrentPair = () => {
    return { type: "CLEAR_CURRENT_PAIR" }
}
