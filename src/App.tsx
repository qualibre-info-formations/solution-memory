import React, {Component} from 'react';
import { BrowserRouter, Route, Routes, Link } from "react-router-dom";
import Memory from './routes/memory';
import HOF from './routes/halloffame';
import { RootState } from './store/configureStore';
import { connect } from 'react-redux';
import {AppDispatch} from './store/configureStore';
import { AnyAction } from 'redux';
import { fetchCards } from './actions/cards';

const VISUAL_PAUSE_MSECS = 750;

interface AppProps {
    cards: string[],
    dispatch: AppDispatch,
    currentPair: number[],
    guesses: number,
    matchedCardIndices: number[]
}

class App extends Component<AppProps> {

    async componentDidMount() {
        // @ts-ignore
        this.props.dispatch(fetchCards());
    }

    handleNewPairClosedBy(index: number) {
        const {currentPair, guesses, matchedCardIndices} = this.props;
        const {cards} = this.props;

        const newPair = [currentPair[0], index];
        const newGuesses = guesses + 1;
        const matched = cards[newPair[0]] === cards[newPair[1]];
        this.setState({currentPair: newPair, guesses: newGuesses});
        if (matched) {
            this.setState({matchedCardIndices: [...matchedCardIndices, ...newPair]});
        }
        setTimeout(() => this.setState({currentPair: []}), VISUAL_PAUSE_MSECS);
    }

    handleCardClick = (index: number) => {
        const {currentPair} = this.props;

        if (currentPair.includes(index)) {
            return;
        }

        if (currentPair.length === 2) {
            return;
        }

        if (currentPair.length === 0) {
            this.setState({currentPair: [index]});
            return;
        }

        this.handleNewPairClosedBy(index);
    }

    render() {
        return (
            <BrowserRouter>
                <h1>Memory</h1>
                <nav
                    style={{
                        borderBottom: "solid 1px",
                        paddingBottom: "1rem",
                    }}
                >
                    <Link to="/halloffame">Hall Of Fame</Link> |{" "}
                    <Link to="/memory">Game</Link>
                </nav>
                <Routes>
                    <Route path="/" element={<Memory />} />
                    <Route path="memory" element={<Memory />} />
                    <Route path="halloffame" element={<HOF />} />
                </Routes>
            </BrowserRouter>)
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        dispatch: (action: AnyAction) => { dispatch(action) }
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        cards: state.cards
    }
}
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(App);